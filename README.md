## sftp-users-with-chroot

Programa escrito em bash que automatiza a criação de usuários sftp com um ambiente chroot jail.

Os seguintes passos são executados pelo programa:

1. Verifica se o grupo **sftpusers** existe e o cria se necessário.

2. Verifica se o **/etc/ssh/sshd.conf** contém a configuração para o chroot, a adiciona ao fim do arquivo caso a config não exista e reinicia o serviço do **sshd**.

3. Verifica se o usuário informado por parâmetro existe e o cria caso necessário com o shell **/bin/false**

4. Verifica se o diretório do usuário para chroot jail existe, o cria caso não exista com os subdiretórios **files** e **dev**. Sendo **files** o diretório padrão para os usuários.

### Como usar

1. Abra um terminal e escale privilégios com o comando: `sudo su`

2. Navegue até o diretório **/opt**: `cd /opt`

3. Faça o clone deste repositório com o seguinte comando:
`git clone https://gitlab.com/iancsn/sftp-users-with-chroot.git`

4. Navegue até o diretório do projeto:
`cd sftp-users-with-chroot`

5. Execute o programa informando por parâmetro o nome do usuário a ser criado: `./sftp-create-user.sh <nome_do_usuario_aqui>`
