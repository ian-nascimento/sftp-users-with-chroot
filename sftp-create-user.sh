#!/bin/bash
#------------------------------------------------------------------------------------------------------
# sftp-create-user.sh - Programa que automatiza a criação de usuários sftp com um ambiente chroot jail.
# Autor: Ian Nascimento <iancarlos37@gmail.com>
#------------------------------------------------------------------------------------------------------

# Variáveis de controle
ARQUIVO_DE_LOG="./sftp-create-user.log"
ARQUIVO_DE_LOCK="./sftp-create-user.lock"
SNIPPET_CONFIGURACAO_DO_CHROOT="sshd-chroot-config-snippet.txt"
ARQUIVO_DE_CONFIGURACAO_DO_SSH_SERVER="/etc/ssh/sshd_config"
usuario=$1
diretorio_para_chroot_jail="/sftp/${usuario}"

# Função para gerar logs em arquivo
gerar_log(){
  printf "$(date "+%b %d %H:%M:%S") $1\n" >> $ARQUIVO_DE_LOG
}

# Verifica se o arquivo de lock existe.
# Caso não exista, o arquivo é gerado e uma trap é definida para ser acionada no fim da execução do script.
if { set -C; 2>/dev/null >${ARQUIVO_DE_LOCK}; }
then
  trap "rm -f $ARQUIVO_DE_LOCK" EXIT
else
  gerar_log "O programa ja esta em execucao... Finalizando"
  exit 1
fi

if ! grep -o -q "sftpusers" /etc/group
then
  gerar_log "Criando o grupo sftpusers"
  echo "Criando o grupo sftpusers"
  groupadd sftpusers
fi

if ! grep -o -q -f ${SNIPPET_CONFIGURACAO_DO_CHROOT} ${ARQUIVO_DE_CONFIGURACAO_DO_SSH_SERVER}
then
  cat ${SNIPPET_CONFIGURACAO_DO_CHROOT} >> ${ARQUIVO_DE_CONFIGURACAO_DO_SSH_SERVER} && \
  gerar_log "Adicionando o snippet no arquivo de configuração do ssh"
  echo "Adicionando o snippet no arquivo de configuração do ssh"
  systemctl restart ssh.service
fi

if ! id -u ${usuario} > /dev/null 2>&1
then
  gerar_log "Criando o usuário ${usuario}"
  useradd -M -s /bin/false ${usuario}
  gpasswd -a ${usuario} sftpusers
else
  gerar_log "O usuário já existe"
  echo "O usuário já existe"
fi

if [ ! -d ${diretorio_para_chroot_jail} ]
then
  mkdir -p "${diretorio_para_chroot_jail}/files" && \
  gerar_log "Criando o diretorio ${diretorio_para_chroot_jail}/files para o usuário ${usuario}: OK"

  mkdir -p -m2755 "${diretorio_para_chroot_jail}/dev" && \
  gerar_log "Criando o diretorio ${diretorio_para_chroot_jail}/dev para o usuário ${usuario}: OK"

  sleep 2

  chown ${usuario}:${usuario} ${diretorio_para_chroot_jail}/files && \
  gerar_log "Tornando o usuário ${usuario} dono do ${diretorio_para_chroot_jail}/files: OK\n"
fi

exit 0
